# ReactJsMaster Project

Discover a sleek and intuitive sidebar crafted with the power of React, React Router, and a touch of clever CSS finesse. Elevate your user experience with a standard fixed sidebar that seamlessly integrates navigation into your web application. (Colors? We've got that covered! 💪)

## Table of Contents

- [Routes](#Routes)
- [Installation](#installation)
- [Usage](#usage)
- [Author](#Author)

## Routes

- /dashboard
- /page-1
- /page-2
- /page-3
- /login
- /register
- /forgotpassword

## Installation

1. Clone the repository.
   ```bash
   git clone git@gitlab.com:amine.guesmi/reactjsmaster.git
2. Installing dependencies.
   ```bash
   npm install

## usage
1. Change the base URL located in /src/utils/ApiConfig.js

2. Run the application, navigate to http://localhost:3000 in your browser, and experience a simple and elegant navigation.
   ```bash
   npm start

## Author
MADE WITH ♥️ BY AMINE GUESMI | 
Email: amine.guesmi@lebackyard.fr
GitHub: [https://github.com/Theamineguesmi](https://github.com/Theamineguesmi)