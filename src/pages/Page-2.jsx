import React from 'react';

function Page2(props) {
    return (
        <div style={{padding: '24px'}}>
            <h1>Page 2</h1>
            <p>I bring the sauce.</p>
        </div>
    );
}

export default Page2;
