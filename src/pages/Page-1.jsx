import React from 'react';
function Page1(props) {
    return (
        <div style={{padding: '24px'}}>
            <h1>Page 1</h1>
            <p>I bring the sauce.</p>
        </div>
    );
}

export default Page1;
