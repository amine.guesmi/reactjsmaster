import { useState } from 'react';
import { useAuth } from '../../utils/AuthContext';

const LoginServices = () => {
  const { login, register } = useAuth();

  const [loginState, setLoginState] = useState({
    phone_number: '',
    password: '',
  });

  const [forgotPasswordState, setForgotPasswordState] = useState({
    email: '',
  });

  const [registrationState, setRegistrationState] = useState({
    username: '',
    email: '',
    password: '',
  });

  const [status, setStatus] = useState({
    error: null,
    message: '',
    loading: false,
  });

  const setPhoneNumber = (phone_number) => {
    setLoginState((prevLoginState) => ({ ...prevLoginState, phone_number }));
  };

  const setPassword = (password) => {
    setLoginState((prevLoginState) => ({ ...prevLoginState, password }));
  };

  const setForgotPasswordEmail = (email) => {
    setForgotPasswordState((prevForgotPasswordState) => ({ ...prevForgotPasswordState, email }));
  };

  const setRegistrationData = (field, value) => {
    setRegistrationState((prevRegistrationState) => ({ ...prevRegistrationState, [field]: value }));
  };

  const setStatusError = (error) => {
    setStatus((prevStatus) => ({ ...prevStatus, error }));
  };

  const setStatusMessage = (message) => {
    setStatus((prevStatus) => ({ ...prevStatus, message }));
  };

  const setStatusLoading = (loading) => {
    setStatus((prevStatus) => ({ ...prevStatus, loading }));
  };

  const handleForgotPassword = async () => {
    try {
      setStatusError(null);
      setStatusLoading(true);

      // Implement your forgot password logic here
      // For example, make an API call to send a reset email

      // Assuming you have successfully sent the reset email
      setStatusMessage('Reset email sent successfully.');

      return { success: true };
    } catch (error) {
      console.error('Forgot password failed:', error.message);
      setStatusError('Forgot password failed. Please try again.');
      return { success: false, error: error.message };
    } finally {
      setStatusLoading(false);
    }
  };

  const handleRegister = async () => {
    try {
      setStatusError(null);
      setStatusLoading(true);

      // Call the register function from AuthContext
      await register({
        username: registrationState.username,
        email: registrationState.email,
        password: registrationState.password,
      });

      // If registration is successful, you can perform additional actions here if needed
      // For example, logging in the user or navigating to a different screen
      // ...

      setStatusMessage('Registration successful.');
      return { success: true };
    } catch (error) {
      console.error('Registration failed:', error.message);
      setStatusError('Registration failed. Please check your information.');
      return { success: false, error: error.message };
    } finally {
      setStatusLoading(false);
    }
  };

  const handleLogin = async () => {
    try {
      setStatusError(null); // Clear any previous error
      setStatusLoading(true); // Set loading to true

      // Call the login function from AuthContext
      await login(loginState.phone_number, loginState.password);

      // If login is successful, you can perform additional actions here if needed
      // For example, fetching user data or navigating to a different screen
      // ...

      return { success: true };
    } catch (error) {
      console.error('Login failed:', error.message);
      setStatusError('Login failed. Please check your credentials.');
      return { success: false, error: error.message };
    } finally {
      setStatusLoading(false); // Set loading to false regardless of success or failure
    }
  };

  return {
    loginState,
    setPhoneNumber,
    setPassword,
    handleLogin,
    forgotPasswordState,
    setForgotPasswordEmail,
    handleForgotPassword,
    registrationState,
    setRegistrationData,
    handleRegister,
    status,
    setStatusError,
    setStatusMessage,
    setStatusLoading,
  };
};

export default LoginServices;
