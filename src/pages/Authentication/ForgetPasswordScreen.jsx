import React from 'react';
import useLoginServices from './AuthServices';
import ClickBack from '../../components/buttons/ClickBack';

const ForgotPasswordScreen = () => {
  const {
    forgotPasswordState,
    setForgotPasswordEmail,
    handleForgotPassword,
    status
  } = useLoginServices();

  const ForgotPasswordForm = () => {
    return (
      
      <form>
        <div className="mb-3">
          <label htmlFor="email" className="form-label">
            Email:
          </label>
          <input
            type="email"
            className="form-control"
            id="email"
            value={forgotPasswordState.email}
            onChange={(e) => setForgotPasswordEmail(e.target.value)}
          />
        </div>
        <ClickBack/>
        <button
          type="button"
          className="btn btn-primary"
          onClick={handleForgotPassword}
          disabled={status.loading}
        >
          {status.loading ? 'Sending Email...' : 'Send Reset Email'}
        </button>

        {status.message && (
          <div className="alert alert-success mt-3" role="alert">
            {status.message}
          </div>
        )}

        {status.error && (
          <div className="alert alert-danger mt-3" role="alert">
            {status.error}
          </div>
        )}
      </form>
    );
  };

  return (
    <div className="container-fluid vh-100 d-flex align-items-center justify-content-center">
    <div className="container-sm mt-5" style={{ maxWidth: '400px' }}>
      <h1 className="text-center">Forgot Password</h1>
      <ForgotPasswordForm />
    </div>
    </div>
  );
};

export default ForgotPasswordScreen;
