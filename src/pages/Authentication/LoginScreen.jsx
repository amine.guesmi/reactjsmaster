import React from 'react';
import LoginServices from './AuthServices';
import ClickBack from '../../components/buttons/ClickBack';
const LoginScreen = () => {
  const {
    loginState,
    setPhoneNumber,
    setPassword,
    handleLogin,
    status
  } = LoginServices();

  const { phone_number, password } = loginState;

  return (
  <div className="container-fluid vh-100 d-flex align-items-center justify-content-center mainBG ">
  <div className="container-sm blurBG p-4 rounded" style={{width:500}}>
      <h1 className="text-center">Login Screen</h1>
      <form>
        <div className="mb-3">
          <label htmlFor="phoneNumber" className="form-label">
            Phone Number:
          </label>
          <input
            type="text"
            className="form-control"
            id="phoneNumber"
            value={phone_number}
            onChange={(e) => setPhoneNumber(e.target.value)}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="password" className="form-label">
            Password:
          </label>
          <input
            type="password"
            className="form-control"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <ClickBack/>
        <button
          type="button"
          className="btn btn-primary"
          onClick={handleLogin}
          disabled={status.loading}
        >
          {status.loading ? 'Logging in...' : 'Login'}
        </button>
        <hr />
        <a href="/forgotpassword">Forget your password ?</a> | <a href="/register">Register</a>

        {status.error && (
          <div className="alert alert-danger mt-3" role="alert">
            {status.error}
          </div>
        )}
      </form>
    </div>
    </div>
  );
};

export default LoginScreen;
