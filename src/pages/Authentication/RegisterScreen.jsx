// RegistrationScreen.jsx
import React from 'react';
import LoginServices from './AuthServices';
import ClickBack from '../../components/buttons/ClickBack';
const RegistrationScreen = () => {
  const {
    registrationState,
    setRegistrationData,
    handleRegister,
    status
  } = LoginServices();

  const RegistrationForm = () => {
    return (
      <form>
        <div className="mb-3">
          <label htmlFor="username" className="form-label">
            Username:
          </label>
          <input
            type="text"
            className="form-control"
            id="username"
            value={registrationState.username}
            onChange={(e) => setRegistrationData('username', e.target.value)}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="email" className="form-label">
            Email:
          </label>
          <input
            type="email"
            className="form-control"
            id="email"
            value={registrationState.email}
            onChange={(e) => setRegistrationData('email', e.target.value)}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="password" className="form-label">
            Password:
          </label>
          <input
            type="password"
            className="form-control"
            id="password"
            value={registrationState.password}
            onChange={(e) => setRegistrationData('password', e.target.value)}
          />
        </div>
        <ClickBack/>
        <button
          type="button"
          className="btn btn-primary"
          onClick={handleRegister}
          disabled={status.loading}
        >
          {status.loading ? 'Registering...' : 'Register'}
        </button>

        {status.message && (
          <div className="alert alert-success mt-3" role="alert">
            {status.message}
          </div>
        )}

        {status.error && (
          <div className="alert alert-danger mt-3" role="alert">
            {status.error}
          </div>
        )}
      </form>
    );
  };

  return (
    <div className="container-fluid vh-100 d-flex align-items-center justify-content-center">
    <div className="container-sm mt-5" style={{ maxWidth: '400px' }}>
      <h1 className="text-center">Registration Screen</h1>
      <RegistrationForm />
    </div>
    </div>
  );
};

export default RegistrationScreen;
