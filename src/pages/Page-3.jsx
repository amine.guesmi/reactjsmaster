import React from 'react';

const Page3 = props => {
    return (
        <div style={{padding: '24px'}}>
            <h1>Page 3</h1>
            <p>I bring the sauce.</p>
        </div>
    );
};

export default Page3;
