import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Page1 from "./pages/Page-1";
import Page2 from "./pages/Page-2";
import Page3 from "./pages/Page-3";
import Dashboard from "./pages/Dashboard";
import NotFound from "./pages/NotFound";
import Layout from "./components/Layout";
//Authentication route
import Log_in from "./pages/Authentication/LoginScreen";
import ForgotPassword from "./pages/Authentication/ForgetPasswordScreen";
import register  from "./pages/Authentication/RegisterScreen";

function Routes() {
    return (
        <BrowserRouter>
          <Switch>
            
            <Route path="/login" component={Log_in} /> {/* Login route */}
            <Route path="/forgotpassword" component={ForgotPassword} /> {/* Login route */}
            <Route path="/register" component={register} /> {/* Login route */}

            <Route render={(props)=>(
                <Layout {...props}>
                    <Switch>
                        <Route path="/" exact component={Dashboard}/>
                        <Route path="/dashboard" exact component={Dashboard}/>
                        <Route path="/page-1" component={Page1}/>
                        <Route path="/page-2" component={Page2}/>
                        <Route path="/page-3" component={Page3}/>
                        <Route component={NotFound}/>
                    </Switch>
                </Layout>
            )}/>
            </Switch>
        </BrowserRouter>
    )
}

export default Routes;
