import React from 'react';
import ClickBack from '../components/buttons/ClickBack';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
function FixedNavBar(props) {
    return (
        <Navbar className="bg-body-tertiary">
        <Container style={{ maxWidth: '1600px' }}>
          <Navbar.Brand><ClickBack/></Navbar.Brand>
          <Navbar.Toggle />
          <Navbar.Collapse className="justify-content-end">
            <Navbar.Text>
              Signed in as: <a href="#login">Amine Guesmi</a>
            </Navbar.Text>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    );
}

export default FixedNavBar;
