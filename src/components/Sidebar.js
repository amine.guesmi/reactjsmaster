import React, { useEffect, useState } from "react";
import SidebarItems from "./SidebarItems";
import { Link } from "react-router-dom";
import '../style/sidebar.css'; // Import the CSS file
import MyLogo from '../images/aminelogo.png';
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

function Sidebar(props, { defaultActive }) {
  const location = props.history.location;
  const lastActiveIndexString = localStorage.getItem("lastActiveIndex");
  const lastActiveIndex = Number(lastActiveIndexString);
  const [activeIndex, setActiveIndex] = useState(lastActiveIndex || defaultActive);
  const [sidebarVisible, setSidebarVisible] = useState(true);

  function changeActiveIndex(newIndex) {
    localStorage.setItem("lastActiveIndex", newIndex);
    setActiveIndex(newIndex);
  }

  function getPath(path) {
    if (path.charAt(0) !== "/") {
      return "/" + path;
    }
    return path;
  }

  useEffect(() => {
    const activeItem = SidebarItems.findIndex(item => getPath(item.route) === getPath(location.pathname));
    changeActiveIndex(activeItem);
  }, [location]);

  const toggleSidebar = () => {
    setSidebarVisible(!sidebarVisible);
    if (!sidebarVisible) {
      document.body.classList.add("no-scroll"); // Optional: Prevent scrolling when sidebar is collapsed
    } else {
      document.body.classList.remove("no-scroll","slideInLeftAnimation");
    }
  };

  const handleResize = () => {
    const isSmallScreen = window.innerWidth <= 768;
    setSidebarVisible(!isSmallScreen);
  };

  useEffect(() => {
    // Add event listener for window resize
    window.addEventListener("resize", handleResize);

    // Initial check for screen size
    handleResize();

    // Cleanup the event listener on component unmount
    return () => {
      window.removeEventListener("resize", handleResize);
      document.body.classList.remove("no-scroll"); 
    };
  }, []); // Empty dependency array to run only once on mount

  return (
    <div className={`sidebar-parent ${sidebarVisible ? 'slideInLeftAnimation' : 'collapsed'}`}>
      <button className="sidebar-toggle-btn btn btn-primary"   type="button" onClick={toggleSidebar}>
      <FontAwesomeIcon icon={faBars} />
      </button>
      {sidebarVisible && (
        <>
          <div className="sidebar">
          <Link to="/">
            <img className="myLogo" src={MyLogo} alt="LogoQuid" />
          </Link>
            {SidebarItems.map((item, index) => (
              <Link key={item.name} to={item.route}>
                <div className={`sidebar-item ${index === activeIndex ? 'active' : ''}`}>
                  <p>{item.name}</p>
                </div>
              </Link>
            ))}
          </div>
          <div className="behind-the-scenes" />
        </>
      )}
    </div>
  );
}

export default Sidebar;
