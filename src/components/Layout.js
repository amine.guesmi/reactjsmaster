import React from 'react';
import Sidebar from "./Sidebar";
import FixedNavBar from "./Nav";
import { AuthProvider } from '../utils/AuthContext';

function Layout(props) {
    return (
        <AuthProvider>
        <div>
            <div style={{display: "flex"}}>
                <Sidebar history={props.history}/>
                <div style={{width:"100%"}}>
                    <FixedNavBar/>
                    {props.children}
                </div>
            </div>
        </div>
        </AuthProvider>
    );
}

export default Layout;
