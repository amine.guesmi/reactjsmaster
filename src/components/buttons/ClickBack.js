import React from 'react';
import { useHistory } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'

const BackButton = () => {
  const history = useHistory();

  const goBack = () => {
    history.goBack();
  };

  return (
    <button 
    onClick={goBack}
    type="button"
    className="btn btn-primary "
    >
    <FontAwesomeIcon icon={faArrowLeft} />
    </button>
  );
};

export default BackButton;
