// apiConfig.js
import axios from 'axios';

const instance = axios.create({
  baseURL: 'Api End point /api/', // Replace with your API base URL
});

// USAGE EXAMPLE
// import api from './ApiConfig'; // Import the configured Axios instance
// const login = async (phone_number, password) => {
//     try {
//       const response = await api.post('/login', {
//         phone_number,
//         password,
//       });
//       const { token } = response.data;
//       setAuthToken(token);
//       localStorage.setItem('authToken', token);
//     } catch (error) {
//       console.error('Login failed:', error.message);
//       throw error;
//     }
//   };
export default instance;