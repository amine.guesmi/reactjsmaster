// AuthContext.js
import React, { createContext, useContext, useState, useEffect } from 'react';
import api from './ApiConfig'; // Import the configured Axios instance

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [authToken, setAuthToken] = useState(null);

  useEffect(() => {
    const storedToken = localStorage.getItem('authToken');
    if (storedToken) {
      setAuthToken(storedToken);
    }
  }, []);

  const login = async (phone_number, password) => {
    try {
      const response = await api.post('/login', {
        phone_number,
        password,
      });
      const { token } = response.data;
      setAuthToken(token);
      localStorage.setItem('authToken', token);
    } catch (error) {
      console.error('Login failed:', error.message);
      throw error;
    }
  };

  const logout = () => {
    setAuthToken(null);
    localStorage.removeItem('authToken');
  };

  return (
    <AuthContext.Provider value={{ authToken, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error('useAuth must be used within an AuthProvider');
  }

  return context;
};
